var Bike = require ('../../models/bike');

exports.bike_list = async function (req, res) {
    console.log ('++ bikeControllerAPI -> bike_list');
    res.json({
        bikes: await Bike.allBikes()
    });
}

exports.bike_create = function (req, res) {
    /*var bike = new Bike (req.body.id, req.body.color, req.body.model);
    bike.location = [ req.body.lat, req.body.lng];
    Bike.add(bike);*/
    var bike = new Bike ({ color: req.body.color, model: req.body.model, location:  [ req.body.lat, req.body.lng]});   
    Bike.add(bike, function (err, result) {
        if (err) console.log (err);
        res.status(200).json({
            bike: result
        });
    });  
}

exports.bike_update = function (req, res){
    console.log("req.query.id: ", req.query.id);
    let newData = { color: req.body.color, model: req.body.model, location:  [ req.body.lat, req.body.lng]};
    Bike.findOnendUpdateByCode(req.query.id, newData ,function(err, bike) {
        if (err) console.log (err);
        console.log("Bike found.", bike);
        //bike.code = req.body.id;
        if (bike != null) {
            bike.color = req.body.color;
            bike.model = req.body.model;
            bike.location = [ req.body.lat, req.body.lng];
            res.status(204).send();
        } else {
            res.status(400).send("Bike id is not valid.");
        }
    });
    
 }

exports.bike_delete = function (req, res) {
    console.log ('++ bikeControllerAPI -> bike_delete');
    Bike.removeByCode(req.body.id, function(err, bike) {
        if (err) console.log (err);
        res.status(204).send();
    });    
}