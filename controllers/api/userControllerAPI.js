var User = require('../../models/user');

exports.user_list = function(req,res){
    User.find({}, function(err, users) {
        res.status(200).json({
            users: users
        })
    })
};

exports.user_create = function(req, res) {
    var user = new User ({ name: req.body.name});
    user.save(function(err) {
        res.status(200).json(user);
    });
};

exports.user_book = function(req, res) {
    User.findById(req.body.id, function(err, user) {
        console.log(user);
        user.book(req.body.bike_id, req.body.from, req.body.to, function(err) {
            console.log('booking!!!');
            res.status(200).send();
        });
    });
};