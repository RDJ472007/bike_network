var mongoose = require('mongoose');
var Booking = require('./booking');
var Schema = mongoose.Schema;

var userSchema = new Schema ({ 
    name: String,
 });

 userSchema.methods.book = function (bikeId, from, to, cb) {
     var booking = new Booking({ user: this.id, bike: bikeId, from: from, to: to});
     console.log(booking);
     booking.save(cb);
 };

 module.exports = mongoose.model('User', userSchema);