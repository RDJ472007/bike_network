var express = require('express');
var router = express.Router();
var userControllerAPI = require('../../controllers/api/userControllerAPI');

router.get('/', userControllerAPI.user_list);
router.post('/create', userControllerAPI.user_create);
router.post('/book', userControllerAPI.user_book);

module.exports = router;
