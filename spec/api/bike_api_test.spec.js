const mongoose = require('mongoose');
var Bike = require ('../../models/bike');
var server = require('../../bin/www');
var request = require ('request');

var base_url = "http://localhost:4000/api/bikes";

describe('Bike API', () => {

    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/bikes_network';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
            //done();
        });        
        done();
    });

    
    afterEach(function(done) {
        Bike.deleteMany({}, function(err, success) { 
            if (err) console.log(err);
            done();
        });
    });

    describe ('GET Bikes', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bikes.length).toBe(0);
                done();
            });
        });
    });

    describe ('POST Bikes /create', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json'};
            var bike1 = '{"id": "10", "color": "red", "model": "traditional", "lat":-34, "lng": -54}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: bike1
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bike = JSON.parse(body).bike;
                console.log(bike);
                expect(bike.color).toBe("red");
                expect(bike.location[0]).toBe(-34);
                expect(bike.location[1]).toBe(-54);
                done();
            });
        });
    });

    describe ('PUT Bikes /update', () => {
        it('Status 204', (done) => {
            var headers = { 'content-type': 'application/json'};
            var bike1 = '{"id": "10", "color": "red", "model": "traditional", "lat":-34, "lng": -54}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: bike1
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bike = JSON.parse(body).bike;
                console.log(bike);
                expect(bike.color).toBe("red");
                expect(bike.location[0]).toBe(-34);
                expect(bike.location[1]).toBe(-54);
                var bike1_updated = '{"id": "11", "color": "blue", "model": "urban", "lat":-37, "lng": -37}';  
                request.put({                    
                    headers: headers,
                    url: base_url + '/update?id=' + bike._id,
                    body: bike1_updated
                }, function(error, response, body) {
                        expect(response.statusCode).toBe(204);
                        done();
                });              
            });
        });
    });

    describe ('DELETE Bikes /delete', () => {
        it('Status 204', (done) => {
            var headers = { 'content-type': 'application/json'};
            var bike1 = '{"id": "10", "color": "red", "model": "traditional", "lat":-34, "lng": -54}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: bike1
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bike = JSON.parse(body).bike;
                console.log(bike);
                expect(bike.color).toBe("red");
                expect(bike.location[0]).toBe(-34);
                expect(bike.location[1]).toBe(-54);
                var bike_id = '{"id": "10"}';  
                request.delete({                    
                    headers: headers,
                    url: base_url + '/delete',
                    body: bike_id
                }, function(error, response, body) {
                        expect(response.statusCode).toBe(204);
                        done();
                });              
            });
        });
    });

    /*describe ('GET Bikes', () => {
        it('Status 200', () => {
            expect(Bike.allBikes.length).toBe(0);
            var bike1 = new Bike ('1','orange','traditional',[7.8877441564647315, -72.50754518930361]);
            Bike.add(bike1);
            request.get('http://localhost:4000/api/bikes', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe ('POST Bikes /create', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json'};
            var bike1 = '{"id": "10", "color": "red", "model": "traditional", "lat": 7.8877441564647315, "lng": -72.50754518930361}';
            request.post({
                headers: headers,
                url: 'http://localhost:4000/api/bikes/create',
                body: bike1
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bike.findById(10).color).toBe("red");
                done();
            });
        });
    });*/

    
});


