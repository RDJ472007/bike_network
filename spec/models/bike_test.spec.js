//const { NotExtended } = require('http-errors');
const mongoose = require('mongoose');
var Bike = require ('../../models/bike');

//jasmine.DEFAULT_TIMEOUT_INTERVAL=7000;

describe ('Testing bikes', function() {
    /*beforeEach(async function() {
        var mongoDB = 'mongodb://localhost/bikes_network';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        await db.on('error', console.error.bind(console, 'connection error'));
        await db.once('open', function() {});
        console.log('We are connected to test database');
    });*/

    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/bikes_network';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
            //done();
        });        
        done();
    });

    /*afterEach(async function() {
        await Bike.deleteMany({});
    });*/

    afterEach(function(done) {
        Bike.deleteMany({}, function(err, success) { 
            if (err) console.log(err);
            done();
        });
    });

    describe('Bike.createInstance', () => {
        it('create a bike instance', () => {
            var bike = Bike.createInstance(1, 'green', 'urban', [-34.5, -54.1]);
            expect(bike.code).toBe(1);
            expect(bike.color).toBe('green');
            expect(bike.model).toBe('urban');
            expect(bike.location[0]).toEqual(-34.5);
            expect(bike.location[1]).toEqual(-54.1);
        });
    });

    /*describe('Bike.allBikes', () => {
        it('start empty', async () => {
            const result = await Bike.allBikes();
            console.log("result2", result);
            console.log("result.length", result.length);
            expect(result.length).toBe(0);               
        });
    });*/

    describe('Bike.allBikes', () => {
        it('start empty', (done) => {
            Bike.allBikes(function(err, bikes) {
                expect(bikes.length).toBe(0);
                done();
            });    
        });
    });

    /*describe('Bike.add', () => {
        it('add a bike', async () => {
            var bike =new Bike({ code: 1, color: 'green', model: 'urban'});
            console.log('bike', bike);
            await Bike.add(bike);
            let bikes = await Bike.allBikes();
            expect(bikes.length).toEqual(1);
            expect(bikes[0].code).toEqual(bike.code);          
        });
    });*/
    
    describe('Bike.add', () => {
        it('add a bike', (done) => {
            var bike =new Bike({ code: 1, color: 'green', model: 'urban'});
            console.log('bike', bike);
            Bike.add(bike, function(err, newBike) {
                Bike.allBikes(function(err, bikes) {
                    expect(bikes.length). toEqual(1);
                    expect(bikes[0].code).toEqual(bike.code);
                    done();
                })
            }); 
        });
    });

    describe('Bike.findByCode', () => {
        it('find bike with code 1', (done) => {
            Bike.allBikes(function(err,bikes){ 
                expect(bikes.length).toBe(0);
                var bike1 = new Bike({ code: 1, color: 'green', model: 'urban'});
                Bike.add(bike1, function(err, newBike) {
                    if (err) console.log(err);
                    var bike2 = new Bike({ code: 2, color: 'red', model: 'urban'});
                    Bike.add(bike2, function (err, targetBike) {
                        if (err) console.log(err);
                        Bike.findByCode(targetBike._id, function(error, bike1) {
                            expect(targetBike.code).toBe(bike1.code);
                            expect(targetBike.color).toBe(bike1.color);
                            expect(targetBike.model).toBe(bike1.model);
                            done();
                        });
                        //done();
                    });
                })
            });
        });
    });

    describe('Bike.removeByCode', () => {
        it('remove bike with code 1', (done) => {
            Bike.allBikes(function(err,bikes){ 
                expect(bikes.length).toBe(0);
                var bike1 = new Bike({ code: 1, color: 'green', model: 'urban'});
                Bike.add(bike1, function(err, newBike) {
                    if (err) console.log(err);
                    var bike2 = new Bike({ code: 2, color: 'red', model: 'urban'});
                    Bike.add(bike2, function (err, targetBike) {
                        if (err) console.log(err);
                        Bike.findByCode(targetBike._id, function(error, bike1) {
                            expect(targetBike.code).toBe(bike1.code);
                            expect(targetBike.color).toBe(bike1.color);
                            expect(targetBike.model).toBe(bike1.model);
                            Bike.removeByCode(bike1._id, function(error, targetBike) {
                                console.log('targetBike', targetBike);
                                expect(targetBike.ok).toBe(1);
                                expect(targetBike.deletedCount).toBe(1);
                                done();
                            });
                        });
                    });
                })
            });
        });
    });

});

// SIN PERSISTENCIA

/*beforeEach(() => {
    Bike.allBikes = [];
});

describe('Bike.allBikes', () => {
    it('start empty', () => {
        expect(Bike.allBikes.length).toBe(0);
    })
});

describe('Bike.add', () => {
    it('adding a bike', () => {
        expect(Bike.allBikes.length).toBe(0);
        var bike1 = new Bike ('1','orange','traditional',[7.8877441564647315, -72.50754518930361]);
        Bike.add(bike1);
        expect(Bike.allBikes.length).toBe(1);
        expect(Bike.allBikes[0]).toBe(bike1);
    })
});

describe('Bike.findById', () => {
    it('should return bike with Id 1', () => {
        expect(Bike.allBikes.length).toBe(0);
        var bike1 = new Bike ('1','orange','traditional',[7.8877441564647315, -72.50754518930361]);
        var bike2 = new Bike ('2','blue','profesional',[7.888401009838708, -72.50255963961185]);
        Bike.add(bike1);
        Bike.add(bike2);
        var targetBike = Bike.findById(1);
        expect(targetBike.id).toBe('1');
        expect(targetBike.color).toBe(bike1.color);
        expect(targetBike.model).toBe(bike1.model);
    })
});

describe('Bike.removeById', () => {
    it('should´t return any bike', () => {
        expect(Bike.allBikes.length).toBe(0);
        var bike1 = new Bike ('1','orange','traditional',[7.8877441564647315, -72.50754518930361]);
        var bike2 = new Bike ('2','blue','profesional',[7.888401009838708, -72.50255963961185]);
        Bike.add(bike1);
        Bike.add(bike2);
        expect(Bike.allBikes.length).toBe(2);
        var deleteTargetBike = Bike.removeById('1');
        expect(Bike.allBikes.length).toBe(1);
    })
});*/