const mongoose = require('mongoose');
var Bike = require ('../../models/bike');
var User = require ('../../models/user');
var Booking = require ('../../models/booking');

describe ('Testing users', function() {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/bikes_network';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
            //done();
        });        
        done();
    });
    
    afterEach(function(done) {
        Booking.deleteMany({}, function(err, success) { 
            if (err) console.log(err);
            User.deleteMany({}, function(err, success) {
                if (err) console.log(err);
                Bike.deleteMany({}, function(err, success) {
                    if (err) console.log(err);
                    done();     
                });                
            })
        });
    });

    describe('a user books a bike', () => {
        it('booking must exist', (done) => {
            const user = new User ({name: 'Richard'});
            user.save();
            const bike = new Bike ({ code: 1, color: "green", model: "urban"});
            bike.save();
            var today = new Date();
            var tomorrow = new Date();
            tomorrow.setDate(today.getDate()+1);
            user.book(bike.id, today, tomorrow, function (err, booking) {
                if (err) console.log(err);
                Booking.find({}).populate('bike').populate('user').exec(function(err, bookings) {
                    if (err) console.log(err);
                    console.log(bookings[0]);
                    expect(bookings.length).toBe(1);
                    expect(bookings[0].bookingDays()).toBe(2);
                    expect(bookings[0].bike.code).toBe(1);
                    expect(bookings[0].user.name).toBe(user.name);
                    done();
                });
                //done();
            });
            //done();
        });
    });

});
